OPENBLASROOT := /home/awild/Projects/openblas/install

OPENBLAS_INCLUDE_DIR := $(OPENBLASROOT)/include
OPENBLAS_LIBRARY_DIR := $(OPENBLASROOT)/lib

SOURCES := gemm.cxx util.cxx test.cxx

OBJECTS := $(patsubst %.cxx,%.o,$(SOURCES))

.PHONY: all clean

all: gemm $(OBJECTS)

gemm: $(OBJECTS) 
	g++ $(OBJECTS) -L$(OPENBLAS_LIBRARY_DIR) -lopenblas -lpthread -o gemm

gemm.o: gemm.cxx
	g++ -I$(OPENBLAS_INCLUDE_DIR) --std=c++11 -o gemm.o -c gemm.cxx
                                                                   
test.o: util.hpp test.hpp test.cxx                                                  
	g++ -I$(OPENBLAS_INCLUDE_DIR) --std=c++11 -o test.o -c test.cxx
                                                                   
util.o: util.hpp util.cxx                                                  
	g++ -I$(OPENBLAS_INCLUDE_DIR) --std=c++11 -o util.o -c util.cxx 

clean:
	rm -f *.o gemm
