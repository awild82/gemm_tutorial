#include "util.hpp"
#include "test.hpp"

namespace GemmTest {

  void reg_gemm(size_t M, size_t N, size_t K, double* A, double* B, double* C) {
    gemm('N', 'N', M, N, K, 1., A, M, B, K, 0., C, M);
  }

  void commutator(size_t N, double* A, double* B, double* C) {
    gemm('N', 'N', N, N, N, 1., B, N, A, N, 0., C, N);
    gemm('N', 'N', N, N, N, 1., A, N, B, N, -1., C, N);
  }

  void unitary_transform(size_t N, dcomplex* U, dcomplex* A, dcomplex* C,
    dcomplex* SCR) {

    // A U**H
    gemm('N', 'N', N, N, N, dcomplex(1.0), U, N, A, N, dcomplex(0.0), 
      SCR, N);
    // U A U**H 
    gemm('N', 'C', N, N, N, dcomplex(1.0), SCR, N, U, N, dcomplex(0.0),
      C, N);
  }

} // namespace GemmTest

int main() {
  GemmTest::printres(GemmTest::test_regular_gemm(&GemmTest::reg_gemm),
    "Regular Multiplication");
  GemmTest::printres(GemmTest::test_commutator(&GemmTest::commutator),
    "Commutator");
  GemmTest::printres(GemmTest::test_unitary_trans(&GemmTest::unitary_transform),
    "Unitary Transform");

  return 0;
}
