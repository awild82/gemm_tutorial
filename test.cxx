#include "test.hpp"

namespace GemmTest {

  const double TOL = 1e-7;

  bool test_regular_gemm(GemmFunc func, bool print) {
    // C = AB
    size_t M = 3;
    size_t N = 4;
    size_t K = 4;
  
    // Allocate memory
    double* A = malloc<double>(M*K);
    double* B = malloc<double>(N*K);
    double* C = malloc<double>(M*N);
  
    // ** A **
    // Set diagonal to 1.0
    for ( auto i = 0; i < M; i++ ) {
      A[i + M*i] = 1.0;
    }
  
    // Set remaining column(s) to 1.0
    for ( auto i = M; i < K; i++ ) {
      for ( auto j = 0; j < M; j++ ) {
        A[j + M*i] = 1.0;
      }
    }
  
    // ** B **
    // Set first column to index+1
    for ( auto i = 0; i < K; i++ ) {
      B[i] = double(i+1);
    }
  
    // C = AB

    func(M, N, K, A, B, C);
  
    if ( print ) {
      printmat("Input A", M, K, A, M);
      printmat("Input B", K, N, B, K);
      printmat("Result", M, N, C, M);
    }
  
    // Validate
    double val = 0;
    for ( auto i = 0; i < M*N; i++ ) {
      if ( i < M ) {
        val += std::abs(C[i] - i - 5);
      } 
      else {
        val += std::abs(C[i]);
      }
    }
  
    // Deallocate memory
    free(A);
    free(B);
    free(C);
  
    return val < TOL;
  }

  bool test_commutator(SquareNoMemFunc func, bool print) {
    // C = [A, B]
    size_t N = 4;
  
    // Allocate memory
    double* A = malloc<double>(N*N);
    double* B = malloc<double>(N*N);
    double* C = malloc<double>(N*N);
  
    // ** A **
    // Set columns to index
    for ( auto i = 0; i < N; i++ ) {
      for ( auto j = 0; j < N; j++ ) {
        A[j + N*i] = double(i + 1);
      }
    }
  
    // ** B **
    // Set first column to index+1
    for ( auto i = 0; i < N; i++ ) {
      B[i] = double(i+1);
    }
  
    // C = AB - BA
    func(N, A, B, C);
  
    if ( print ) {
      printmat("Input A", N, N, A, N);
      printmat("Input B", N, N, B, N);
      printmat("Result", N, N, C, N);
    }
  
    // Validate
    double val = 0;
    for ( auto i = 0; i < N; i++ ) {
      for ( auto j = 0; j < N; j++ ) { 
        if ( i == 0 ) {
          val += C[j + N*i] - 29 + i;
        } 
        else {
          val += C[j + N*i] - j*N*i;
        }
      }
    }
  
    // Deallocate memory
    free(A);
    free(B);
    free(C);
  
    return (val < TOL);
  
  }

  bool test_unitary_trans(SquareMemFunc func, bool print) {
    // C = U A U**H
  
    const size_t N = 4;
  
    bool result = true;
  
    dcomplex* U = malloc<dcomplex>(N*N);
    dcomplex* A = malloc<dcomplex>(N*N);
    dcomplex* C = malloc<dcomplex>(N*N);
    dcomplex* SCR = malloc<dcomplex>(N*N);
  
    U[0]  = dcomplex(-0.70278608,  0.40185159);
    U[1]  = dcomplex( 0.08889366, -0.35689377);
    U[2]  = dcomplex( 0.09570149, -0.21056005);
    U[3]  = dcomplex( 0.28285465,  0.27537378);
  
    U[4]  = dcomplex( 0.04441475, -0.37067885);
    U[5]  = dcomplex(-0.49886563, -0.14169260);
    U[6]  = dcomplex(-0.23405631,  0.30621451);
    U[7]  = dcomplex( 0.5300376 ,  0.40272976);
  
    U[8]  = dcomplex( 0.02535389,  0.36677475);
    U[9]  = dcomplex(-0.15887578, -0.00763141);
    U[10] = dcomplex( 0.42106905,  0.68838807);
    U[11] = dcomplex(-0.32634232,  0.28610682);
  
    U[12] = dcomplex(-0.1384189 , -0.22562183);
    U[13] = dcomplex(-0.62419138, -0.42528392);
    U[14] = dcomplex( 0.29910031, -0.23941121);
    U[15] = dcomplex(-0.34140457, -0.31002888);
  
  
    // Form identity
    for ( auto i = 0; i < N; i++ ) {
      A[i + N*i] = dcomplex(1.0, 0.0);
    }
  
    // Transform identity
    // Be careful of the "operands unchanged" specs of A and B - need to use SCR
    func(N, U, A, C, SCR);
  
    if (print) {
      printmat("Unitary", N, N, U, N);
      printmat("Identity", N, N, A, N);
      printmat("Result", N, N, C, N);
    }
  
    // Verify that U**H I U = I
    double scratch;
    for ( auto i = 0; i < N; i++ ) {
      for ( auto j = 0; j < N; j++ ) {
        if ( i == j )
          scratch = std::abs(C[j + N*i] - dcomplex(1.0, 0.0));
        else
          scratch = std::abs(C[j + N*i]);
        result &= scratch < TOL;
      }
    }
  
    // Form non-trivial case
    // Column major
    for ( auto i = 0; i < N; i++ ) {
      for (auto j = 0; j < N; j++ ) {
        A[N*j + i] = j + 1;
      }
    }
  
    // Transform non-trivial case
    func(N, U, A, C, SCR);

    SCR[0]  = dcomplex( 0.81766393, -0.29739436);
    SCR[1]  = dcomplex( 1.43425142,  0.84778292);
    SCR[2]  = dcomplex(-0.71224908, -0.51207831);
    SCR[3]  = dcomplex(-0.25111376, -0.69352259);
                       
    SCR[4]  = dcomplex( 2.58746258, -2.49292172);
    SCR[5]  = dcomplex( 6.83401062,  0.79559125);
    SCR[6]  = dcomplex(-3.54641484, -0.73879501);
    SCR[7]  = dcomplex(-2.11017489, -2.19651479);
                       
    SCR[8]  = dcomplex(-1.35003349,  1.52407467);
    SCR[9]  = dcomplex(-3.89610585, -0.14346845);
    SCR[10] = dcomplex( 2.03642192,  0.25856892);
    SCR[11] = dcomplex( 1.29036093,  1.14632508);
                       
    SCR[12] = dcomplex( 0.89322186,  0.36664226);
    SCR[13] = dcomplex( 0.5439303 ,  1.76707956);
    SCR[14] = dcomplex(-0.20209444, -0.95227619);
    SCR[15] = dcomplex( 0.31190353, -0.75676581); 

    for ( auto i = 0; i < N*N; i++ ) {
        result &= std::abs(C[i] - SCR[i]) < TOL; 
    }
  
    if (print) {
      printmat("Unitary", N, N, U, N);
      printmat("Input", N, N, A, N);
      printmat("Result", N, N, C, N);
      printmat("Expect", N, N, SCR, N);
    }

    return result;
  }

} // namespace GemmTest


