#ifndef __INCLUDED_TEST_HEADER__
#define __INCLUDED_TEST_HEADER__

#include "util.hpp"

#include <functional>

typedef std::function<void(size_t, size_t, size_t, double*, double*, double*)> GemmFunc;
typedef std::function<void(size_t, double*, double*, double*)> SquareNoMemFunc;
typedef std::function<void(size_t, dcomplex*, dcomplex*, dcomplex*, dcomplex*)> SquareMemFunc;

namespace GemmTest {

  bool test_regular_gemm(GemmFunc func, bool print = false);
  bool test_commutator(SquareNoMemFunc func, bool print = false);
  bool test_unitary_trans(SquareMemFunc func, bool print = false);

} // namespace GemmTest

#endif
