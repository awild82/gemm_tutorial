#include "util.hpp"

template <>
double* GemmTest::malloc(const size_t num);

template <>
dcomplex* GemmTest::malloc(const size_t num);

void GemmTest::printres(bool res, std::string msg = "") {
  if ( res ) std::cout << "[\x1b[32mPASS\x1b[0m] " << msg << std::endl;
  else std::cout << "[\x1b[31mFAIL\x1b[0m] " << msg << std::endl;
}

void GemmTest::gemm(char TRANSA, char TRANSB, int M, int N, int K, double ALPHA, double*
            A, int LDA, double* B, int LDB, double BETA, double* C, int LDC) {
  dgemm_(&TRANSA, &TRANSB, &M, &N, &K, &ALPHA, A, &LDA, B, &LDB, &BETA, C,
      &LDC);
}


void GemmTest::gemm(char TRANSA, char TRANSB, int M, int N, int K, dcomplex ALPHA,
            dcomplex* A, int LDA, dcomplex* B, int LDB, dcomplex BETA,
            dcomplex* C, int LDC) {
  zgemm_(&TRANSA, &TRANSB, &M, &N, &K, reinterpret_cast<double*>(&ALPHA),
      reinterpret_cast<double*>(A), &LDA, reinterpret_cast<double*>(B), &LDB,
      reinterpret_cast<double*>(&BETA), reinterpret_cast<double*>(C), &LDC);
}

