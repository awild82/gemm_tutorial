#ifndef __INCLUDED_UTIL_HEADER__
#define __INCLUDED_UTIL_HEADER__

#include <complex>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <string>

#include <f77blas.h>

typedef std::complex<double> dcomplex;

namespace GemmTest {
  template <typename T>
  T* malloc(const size_t num) {
    void* ptr = ::malloc(num*sizeof(T));
    memset(ptr, 0, num*sizeof(T));
    return static_cast<T*>(ptr);
  }

  template <typename T>
  void printmat(std::string name, const size_t M, const size_t N, T* A, const size_t LDA) {

    std::cout << "-----  " << name << "  -----" << std::endl;
  
    std::cout << std::scientific << std::left << std::setprecision(4);
  
    std::cout << std::setw(5) << " ";
    for ( auto i = 0; i < N; i++ )
      std::cout << std::setw(12) << std::right << i;
  
    std::cout << std::endl;
  
    // COLUMN MAJOR
    for ( auto i = 0; i < M; i++ ) {
  
      std::cout << std::setw(5) << std::left << i;
      std::cout << std::right;
  
      for ( auto j = 0; j < N; j++ ) {
        std::cout << std::setw(12) << A[j*LDA + i];
      } 
  
      std::cout << std::endl;
    }
  
    std::cout << std::endl;
  }
  
  void gemm(char TRANSA, char TRANSB, int M, int N, int K, double ALPHA, double*
              A, int LDA, double* B, int LDB, double BETA, double* C, int LDC);
  
  void gemm(char TRANSA, char TRANSB, int M, int N, int K, dcomplex ALPHA,
              dcomplex* A, int LDA, dcomplex* B, int LDB, dcomplex BETA,
              dcomplex* C, int LDC);
  
  void printres(bool res, std::string msg);
}

#endif
